<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Telerik.Sitefinity.Search</name>
    </assembly>
    <members>
        <member name="T:Telerik.Sitefinity.Services.Search.Data.IDocument">
            <summary>
            Represents data used by the search service to store it in a search engine.
            </summary>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.Data.IDocument.GetValue(System.String)">
            <summary>
            Returns the value of the given field.
            </summary>
            <param name="filedName">Name of the filed.</param>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.Data.IDocument.SetValue(System.String,System.String)">
            <summary>
            Sets the given value to the specified field.
            </summary>
            <param name="filedName">Name of the filed.</param>
            <param name="fieldValue">The field value.</param>
        </member>
        <member name="P:Telerik.Sitefinity.Services.Search.Data.IDocument.IdentityField">
            <summary>
            Gets the identity field.
            </summary>
            <value>The identity field.</value>
        </member>
        <member name="P:Telerik.Sitefinity.Services.Search.Data.IDocument.Fields">
            <summary>
            Gets the fields to be stored.
            </summary>
            <value>The fields.</value>
        </member>
        <member name="T:Telerik.Sitefinity.Services.Search.Data.IField">
            <summary>
            Represents atomic data used by the IDocument interface
            </summary>
        </member>
        <member name="P:Telerik.Sitefinity.Services.Search.Data.IField.Name">
            <summary>
            Gets or sets the name of the field.
            </summary>
            <value>The name of the field.</value>
        </member>
        <member name="P:Telerik.Sitefinity.Services.Search.Data.IField.Value">
            <summary>
            Gets or sets the value of the field.
            </summary>
            <value>The value of the field.</value>
        </member>
        <member name="P:Telerik.Sitefinity.Services.Search.Data.IField.Attributes">
            <summary>
            Gets the attributes of the field.
            </summary>
            <value>The attributes of the field.</value>
        </member>
        <member name="T:Telerik.Sitefinity.Services.Search.ISearchQueryBuilder">
            <summary>
            Provides the common contract for building a search query and converting it as a string
            </summary>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchQueryBuilder.GetQuery">
            <summary>
            Gets the query converted to string.
            </summary>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchQueryBuilder.AddTerm(System.String,System.String)">
            <summary>
            Adds a term to the query with the specified field and text.
            </summary>
            <param name="field">The field.</param>
            <param name="text">The text.</param>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchQueryBuilder.AddTerm(System.String,System.String,System.Single)">
            <summary>
            Adds a term to the query with the specified field, text, and boost.
            </summary>
            <param name="field">The field.</param>
            <param name="text">The text.</param>
            <param name="boost">The boost.</param>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchQueryBuilder.CreateGroup(Telerik.Sitefinity.Services.Search.QueryOperator)">
            <summary>
            Creates a new group of terms.
            </summary>
            <param name="queryOperator">The query operator used to join the terms inside the group.</param>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchQueryBuilder.CloseGroup">
            <summary>
            Closes the group of terms.
            </summary>
            <param name="queryOperator">The query operator used to join the group to the other groups.</param>
        </member>
        <member name="T:Telerik.Sitefinity.Services.Search.QueryOperator">
            <summary>
            Represents the types of operators used to build a search query.
            </summary>
        </member>
        <member name="F:Telerik.Sitefinity.Services.Search.QueryOperator.And">
            <summary>
            The logical AND operator
            </summary>
        </member>
        <member name="F:Telerik.Sitefinity.Services.Search.QueryOperator.Or">
            <summary>
            The logical OR operator
            </summary>
        </member>
        <member name="T:Telerik.Sitefinity.Services.Search.ISearchService">
             <summary>
            The contract for search operations that provide the ability to create search catalogues, index and returns the documents matching search query.
             </summary>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.Search(System.String,System.String,System.String[],System.String[],System.Object[])">
            <summary>
            Searches the specified catalogue and returns the document matching the query.
            </summary>
            <param name="catalogueName">The name of the catalogue to search.</param>
            <param name="query">The query.</param>
            <param name="highlightedFields">
            The fields to be used to generate a summary with highlighted text.
            If highlightedFields is <c>null</c> then the default field names are "Title" and "Content".
            Pass an empty array if you want the summary to be empty.
            </param>
            <param name="orderBy">
            The names of the fields by which the result will be ordered.
            "_score" is a special name that orders the document by their relevance. Actual fields should not use this name.
            If orderBy is omitted the default sorting is "_score".
            If the name of a field ends with " desc" than the order will be reversed for this field.
            </param>
            <param name="additionalParams">The additional parameters to the search query.</param>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.Search(System.String,System.String,System.String[],System.Int32,System.Int32,System.String[],System.Object[])">
            <summary>
            Searches the specified catalogue and returns the document matching the query.
            </summary>
            <param name="catalogueName">The name of the catalogue to search.</param>
            <param name="query">The query.</param>
            <param name="highlightedFields">
            The fields to be used to generate a summary with highlighted text.
            If highlightedFields is <c>null</c> then the default field names are "Title" and "Content".
            Pass an empty array if you want the summary to be empty.
            </param>
            <param name="skip">Bypasses the specified number of documents in the result set.</param>
            <param name="take">Returns the specified number of documents from the result set.</param>
            <param name="orderBy">
            The names of the fields by which the result will be ordered.
            "_score" is a special name that orders the document by their relevance. Actual fields should not use this name.
            If orderBy is omitted the default sorting is "_score".
            If the name of a field ends with " desc" than the order will be reversed for this field.
            </param>
            <param name="additionalParams">The additional parameters to the search query.</param>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.UpdateCatalogue(System.String,System.Collections.Generic.IEnumerable{Telerik.Sitefinity.Services.Search.Data.IDocument})">
            <summary>
            Adds or updates the provided documents in the specified catalogue.
            </summary>
            <param name="catalogueName">Name of the catalogue.</param>
            <param name="data">A collection of documents to add or update.</param>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.SearchIndexExists(System.String)">
            <summary>
            Return true if the catalog exists.
            </summary>
            <param name="catalogueName">Name of the catalogue.</param>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.RemoveDocuments(System.String,System.Collections.Generic.IEnumerable{Telerik.Sitefinity.Services.Search.Data.IDocument})">
            <summary>
            Removes the provided documents from the specified catalogue.
            </summary>
            <param name="catalogueName">The name of the catalogue.</param>
            <param name="data">The collection of documents to remove.</param>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.RemoveDocument(System.String,Telerik.Sitefinity.Services.Search.Data.IField)">
            <summary>
            Removes the document with the specified identity from the specified catalogue.
            </summary>
            <param name="catalogueName">The name of the catalogue.</param>
            <param name="identityField">The identity field.</param>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.DeleteCatalogue(System.String)">
            <summary>
            Deletes the specified catalogue.
            </summary>
            <param name="catalogueName">The name of the catalogue.</param>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.CreateDocument(System.Collections.Generic.IEnumerable{Telerik.Sitefinity.Services.Search.Data.IField},System.String)">
            <summary>
            Creates a document that can be used by the service.
            </summary>
            <param name="fields">The fields.</param>
            <param name="identityField">The identity field.</param>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.CreateDocumentField">
            <summary>
            Creates a document field that can be used by the service.
            </summary>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.CreateQueryBuilder">
            <summary>
            Creates a search query builder that can be used to generate a query for the service.
            </summary>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchService.EscapeQuery(System.String)">
            <summary>
            Escapes the query.
            </summary>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchServiceExtensions.BuildQuery(Telerik.Sitefinity.Services.Search.ISearchService,System.String,System.Collections.Generic.IEnumerable{System.String},System.Boolean)">
            <summary>
            Builds a query using the given text and fields.
            </summary>
            <param name="service">The search service.</param>
            <param name="text">The text to be searched for.</param>
            <param name="searchFields">The fields that will be used for the search.</param>
            <param name="addMultilingualCheck">if set to <c>true</c> adds additional terms for the "Language" field.</param>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchServiceExtensions.CreateDocumentField(Telerik.Sitefinity.Services.Search.ISearchService,System.String,System.String,System.Collections.Generic.IDictionary{System.String,System.String})">
            <summary>
            Creates a document field.
            </summary>
            <param name="service">The search service.</param>
            <param name="name">The field name.</param>
            <param name="value">The field value.</param>
            <param name="attributes">The field attributes.</param>
            <returns></returns>
        </member>
        <member name="M:Telerik.Sitefinity.Services.Search.ISearchServiceExtensions.TransformLanguageFieldValue(System.String)">
            <summary>
            Transforms the value of the language field to a tokenizable string.
            </summary>
            <param name="language">The language field value.</param>
            <returns></returns>
        </member>
    </members>
</doc>
